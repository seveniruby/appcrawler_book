# 动作触发器



## 启动脚本
用于划过开屏的各种操作. 遍历开始前会先运行这个命令序列. 目前默认就会尝试滑动 你可以用这个配置作微调.
```json
"startupActions" : [ 
  "scroll left", 
  "scroll left", 
  "scroll left", 
  "scroll left", 
  "scroll down" 
]
```
## 触发配置
idOrName 表示只要是id或者text属性匹配到了给定的值, 就会触发action的动作.  
idOrName支持严格正则表达式. 比如某个按钮的文本是```功能搬到这里啦```的提示控件可以通过```.*这里.*```来匹配到.  
action支持如下动作  
```
click
scroll
scroll left
scroll up
scroll down
任意文本非上述文本会被当做输入
```
times表示规则被应用几次后删除, 如果是0表示永久生效.

## 控件触发器示例

action如果是click就是点击. 如果是非click 就认为是输入内容.
idOrName可以是xpath表达式
```
"elementActions" : [ {
    "action" : "click",
    "idOrName" : "登 录",
    "times" : 2
  }, {
    "action" : "click",
    "idOrName" : "登录",
    "times" : 2
  }, {
    "action" : "15600534760",
    "idOrName" : "account",
    "times" : 1
  },{
    "action" : "scroll left",
    "idOrName" : "专题",
    "times" : 1
  },
    {
      "action" : "scroll down",
      "idOrName" : "专题",
      "times" : 1
    },
    {
    "action" : "xxxxxxx",
    "idOrName" : "password",
    "times" : 1
  }, {
    "action" : "click",
    "idOrName" : "button_next",
    "times" : 1
  }, {
    "action" : "click",
    "idOrName" : "稍后再说",
    "times" : 0
  }, {
    "action" : "click",
    "idOrName" : "这里可以.*",
    "times" : 0
  }
  ]
```
