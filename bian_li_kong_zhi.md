# 遍历控制
配置选项的作用可参考项目目录下的yaml格式的配置文件, 里面有详细的注释解释每个配置项的作用. 

### 唯一性
appcrawler试图用接口测试的理念来定义app遍历测试. 每个app界面认为是一个url. 默认是用当前的activity名字或者navigatorbar来表示. 这个可以通过defineUrl配置实现自定义. 

控件的唯一性通过配置项的xpathAttributes来定义. 默认是通过基本属性iOS的为name label value path, Androd的为resource-id content-desc text index这几个属性来唯一定义一个元素. 可以通过修改这个配置项实现自定义. 

url的定义是一门艺术, 可以决定如何优雅快速有效的遍历
### XPath的作用
appcrawler大量的使用XPath来表示元素的范围. 大部分的选项都可以通过XPath来指定范围. 比如黑白名单, 遍历顺序等

### 遍历顺序控制
适用于在一些列表页或者tab页中精确的控制点击顺序  
selectedList表示要遍历的元素特征  
firstList表示优先遍历元素特征  
lastList表示最后应该遍历的元素特征  
统一使用XPath来表示  

**需要注意的是firstList和lastList指定的元素必须包含在selectedList中**
