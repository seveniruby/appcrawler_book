# 代理插件
自动获取app上每次点击对应的网络请求. 支持http和https
## 安装
目前是默认自带.

## 启用
在配置文件中加入插件
```
  "pluginList" : [
    "com.xueqiu.qa.appcrawler.plugin.TagLimitPlugin",
    "com.xueqiu.qa.appcrawler.plugin.ProxyPlugin"
  ],
```

代理插件默认开启7771端口.  
配置你的Android或者iOS的设备的代理. 指向你当前运行appcrawler的机器和7771端口  
## 结果
在做每个点击的时候都会保存这期间发送的请求. 也就是记录前后两次点击中间的所有通过代理的请求.  
最后会在结果目录里面生成后缀名为har的文件.  
